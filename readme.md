Для роботи проекту потрібно прописати в термінал команду "npm i" для встановлення модулів, потім команду "gulp".

Для входу в профіль э 3 юзера:
email: "admin@gmail.com", pass: "12345"
email: "doctor@gmail.com", pass: "11111"
email: "medic@gmail.com", pass: "54321"
Кожен юзер має свій токен і у кожного свої картки.

Над проектом працювали: Гончаров Владислав @Vladyslav-Honcharov, Мужецька Катерина @mujetskaya2010, Антон Школяренко @Pingvinmd

@Vladyslav-Honcharov: створення функції логіну, класів Visit, VisitDentist,VisitCardiologist,VisitTerapust, функція фільтрації,функція створення карток,модальні вікна їх валідація, fetch запити

@mujetskaya2010: створення header, розміщення та дизайн блоків та стилей, функція showMore, функція відображення карток на сторінці. функція створення візиту, редагування карток, валідація форми

@Pingvinmd: створення збірки, функція редагування карток, функія видалення, валідація логіна.

GitLab Pages: https://vladyslav-honcharov.gitlab.io/step_project_cards
